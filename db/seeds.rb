Restaurant.delete_all
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Restaurant.delete_all
lines = File.new(Rails.root.join('db', 'restaurantdata.csv')).readlines
header = lines.shift.strip
keys = header.split('|')
lines.each do |line|
	values = line.strip.split('|')
	attributes = Hash[keys.zip values]
	Restaurant.create(attributes)
end
