class CreateRestaurants < ActiveRecord::Migration
  def change
    create_table :restaurants do |t|
      t.string :name
      t.string :location
      t.integer :up_votes
      t.integer :down_votes

      t.timestamps null: false
    end
  end
end
