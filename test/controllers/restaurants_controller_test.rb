require 'test_helper'

class RestaurantsControllerTest < ActionController::TestCase
  setup do
    @restaurant = restaurants(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:restaurants)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create restaurant" do
    assert_difference('Restaurant.count') do
      post :create, restaurant: { down_votes: @restaurant.down_votes, location: @restaurant.location, name: @restaurant.name, up_votes: @restaurant.up_votes }
    end

    assert_redirected_to restaurant_path(assigns(:restaurant))
  end

  test "should show restaurant" do
    get :show, id: @restaurant
    assert_response :success
  end

  test "should update restaurant" do
    patch :update, id: @restaurant, restaurant: { down_votes: @restaurant.down_votes, location: @restaurant.location, name: @restaurant.name, up_votes: @restaurant.up_votes }
    assert_redirected_to restaurant_path(assigns(:restaurant))
  end
  
  test "should find restaurant by name" do
    result = Restaurant.search("Texas")
    expected = restaurants(:three).name
    actual = result.pluck(:name)[0]
    
    assert_equal(expected, actual)
  end
  
  test "should not find restaurant by name" do
    result = Restaurant.search("FakeName")
    
    assert(result.blank?)
  end
  
  test "should find restaurant by location" do
    result = Restaurant.search("atlanta")
    expected = restaurants(:five).location
    actual = result.pluck(:location)[0]
    
    assert_equal(expected, actual)
  end
  
  test "should not find restaurant by location" do
    result = Restaurant.search("FakeStreet")
    
    assert(result.blank?)
  end
  
  test "should find restaurant by name and location" do
    result = Restaurant.search("Texas")
    expected_by_name = restaurants(:three).name
    expected_by_location = restaurants(:seven).location
    actual_by_name = result.pluck(:name)[0]
    actual_by_location = result.pluck(:location)[1]
    
    assert_equal(expected_by_name, actual_by_name)
    assert_equal(expected_by_location, actual_by_location)
  end
  
  test "should not find restaurant by name and location" do
    result_by_name = Restaurant.search("FakeName")
    assert(result_by_name.blank?)
    
    result_by_location = Restaurant.search("FakeStreet")
    assert(result_by_location.blank?)
  end

  test "should upvote restaurant" do
    assert_difference('Vote.count') do    
      post :upvote, id: @restaurant, restaurant: { down_votes: @restaurant.down_votes, location: @restaurant.location, name: @restaurant.name, up_votes: @restaurant.up_votes }
      end

    assert_redirected_to @restaurant
  end

  test "should downvote restaurant" do
    assert_difference('Downvote.count') do  
      post :downvote, id: @restaurant, restaurant: { down_votes: @restaurant.down_votes, location: @restaurant.location, name: @restaurant.name, up_votes: @restaurant.up_votes }
    end

    assert_redirected_to @restaurant
  end
end
