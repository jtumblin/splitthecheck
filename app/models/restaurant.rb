class Restaurant < ActiveRecord::Base
  has_many :votes, dependent: :destroy
  has_many :downvotes, dependent: :destroy

  def self.search(search)
    where('name LIKE ? OR location LIKE ?', "%#{search}%", "%#{search}%")
  end
end
