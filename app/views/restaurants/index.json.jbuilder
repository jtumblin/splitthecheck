json.array!(@restaurants) do |restaurant|
  json.extract! restaurant, :id, :name, :location, :up_votes, :down_votes
  json.url restaurant_url(restaurant, format: :json)
end
